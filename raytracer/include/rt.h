/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:37:53 by jfuster           #+#    #+#             */
/*   Updated: 2016/05/11 12:12:38 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_H
# define RT_H
# include <mlx.h> 
# include "../libft/includes/libft.h"
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <fcntl.h>
# include <stdio.h>
# include <pthread.h>

# define ANSI_COLOR_BLUE    "\033[34m"
# define ANSI_COLOR_RED     "\033[31m"
# define KEY_ESC 	53
# define KEY_LEFT 	123
# define KEY_RIGHT 	124
# define KEY_UP 	126
# define KEY_DOWN 	125
# define SCR_X	1000
# define SCR_Y	1000
# define SPHERE		1
# define CONE		2
# define PLANE		3
# define CYLINDER	4
# define PARABOLOID	5
# define LIGHT		6
# define CAMERA		7
# define DEPTH	3
# define THREAD_LIMIT	1000

typedef struct	s_vec
{
	double				x;
	double				y;
	double				z;
}				t_vec;

typedef struct	s_camera
{
	t_vec				pos;
	t_vec				rot;

	double				h_angle;
	double				v_angle;
}				t_camera;

typedef struct	s_light
{
	t_vec				pos;
	t_vec				rot;
	int					color;

	struct	s_light		*next;
	struct	s_light		*last;
}				t_light;

typedef struct	s_ray
{
	t_vec				origin;
	t_vec				dir;
}				t_ray;

typedef struct	s_obj
{
	int					type;

	t_vec				pos;
	t_vec				dir;
	t_vec				rot;

	double				angle;
	double				radius;
	double				slide;

	int					color;

	struct s_obj		*next;
	struct s_obj		*last;
}				t_obj;

typedef struct	s_collision
{
	t_ray				ray;
	t_vec				ray_dir;
	t_vec				ray_or;

	t_obj				*object;
	void				(*diffuse)();

	t_vec				impact;
	t_vec				normale;
	double				impact_d;
	double				dist_tolight;

	double				light_ratio;
	double				spec_ratio;
	int					final_color;

	int					light_color;

	struct s_collision	*next;
}				t_collision;

typedef struct	s_env
{
	void				*img_ptr;
	char				*img_data;
	unsigned char		*img_data_aa;
	void				*mlx_ptr;
	void				*win_ptr;

	int					sl;
	int					bpp;
	int					end;

	double				ambiant_light;
	t_camera			*camera;
	t_light				*light;
	t_ray				*ray;
	t_obj				*obj;
	t_collision			*clsn;
	t_collision			*rflct;
	pthread_mutex_t		mutex;

	int					nbr_lights;
	int					count_type;

	int					aa;
}				t_env;

typedef struct	s_thread_data
{
	t_env				*e;
	
	int					y_start;
	int					y_end;
}				t_thread_data;

typedef struct	s_thr
{
	pthread_t			thr_id[THREAD_LIMIT];
}				thr_t;

/*
**	main.c
*/
void			init(t_env *e);
int				expose_hook(t_env *e);
int				key_hook(int keycode, t_env *e);

/*
**	list.c
*/
// void			add_camera(t_env *e);
void			add_obj(t_env *e, int type);
void			add_light(t_env *e);

/*
**	impact.c
*/
int				impact(t_env *e, t_ray *ray, t_collision *clsn);
int				impact_sphere(t_obj *ptr, t_ray *ray, t_collision *clsn);
int				impact_plane(t_obj *ptr, t_ray *ray, t_collision *clsn);
int				impact_cylinder(t_obj *ptr, t_ray *ray, t_collision *clsn);
int				impact_cone(t_obj *ptr, t_ray *ray, t_collision *clsn);
int				impact_paraboloid(t_obj *ptr, t_ray *ray, t_collision *clsn);

/*
**	sub_impact.c
*/
int				sub_impact(t_env *e, t_vec *origin, t_vec *dir, t_collision *clsn);
int				sub_impact_sphere(t_obj *obj, t_vec *origin, t_vec *dir, t_collision *clsn);
int				sub_impact_plane(t_obj *obj, t_vec *origin, t_vec *dir, t_collision *clsn);
int				sub_impact_cylinder(t_obj *obj, t_vec *origin, t_vec *dir, t_collision *clsn);
int				sub_impact_cone(t_obj *obj, t_vec *origin, t_vec *dir, t_collision *clsn);

/*
**	diffuse.c
*/
double			apply_specular(t_env *e, t_light *light, t_collision *clsn);
double			apply_diffuse(t_env *e, t_light *light, t_collision *clsn);
void			lighting_function(t_env *e, t_collision *clsn);

/*
**	normale.c
*/
void			calculate_normale(t_collision *clsn, int shape, t_ray *ray);

/*
**	trace.c
*/
int				get_color(t_env *e, int color, t_collision *clsn);
void			get_ray_dir(t_env *e, t_vec *vec, int x, int y);
void			draw_pixel(t_env *e, t_collision *clsn);
void			trace(t_env *e);

/*
**	math.c
*/
double			quadratic(double tab[3]);
void			fill_collision(t_obj *ptr, double dist, t_collision *clsn, t_ray *ray);

/*
**	vectors.c
*/
void			set_vec(t_vec *vec, double x, double y, double z);
double			magnitude(t_vec *vec);
t_vec			normalize(t_vec *vec);
t_vec			sub_vec(t_vec *vec1, t_vec *vec2);
t_vec			reflect_ray(t_vec *ray, t_vec *normale);
double			dot(t_vec *a, t_vec *b);


/*
**	parser.c
*/
void			parse_file(t_env *e, char *av);
// int				open_file(int argc, char *file);
// void			parse(t_env *e, int argc, char *file);

/*
**	tools.c
*/
void			usage(char *error);
void			ft_blue(char *s);
void			ft_error(char *s, int fd);
void			quit_program(char *str, t_env *e);
unsigned int				construct_color(unsigned char r, unsigned char g, unsigned char b);
void			put_pixel_to_img(t_env *e, int x, int y, int color);


void	mlx_put_image_to_win(t_env *e);
unsigned char get_aa_color(t_env *e, int x, int y, int offset);

#endif
