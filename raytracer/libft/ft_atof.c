/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 09:32:52 by jfuster           #+#    #+#             */
/*   Updated: 2016/04/19 11:26:54 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

static void	init_value(t_info *env)
{
	env->a = 0.0;
	env->e = 0;
	env->sign = 1;
	env->i = 0;
}

static void	check_power(t_info *env, char *s)
{
	if (env->c == '.')
		while ((env->c = *s++) != '\0' && ft_isdigit(env->c))
		{
			env->a = env->a * 10.0 + (env->c - '0');
			env->e = env->e - 1;
		}
	if (env->c == 'e' || env->c == 'E')
	{
		env->c = *s++;
		if (env->c == '+')
			env->c = *s++;
		else if (env->c == '-')
		{
			env->c = *s++;
			env->sign = -1;
		}
		while (ft_isdigit(env->c))
		{
			env->i = env->i * 10 + (env->c - '0');
			env->c = *s++;
		}
		env->e += env->i * env->sign;
	}
}

double		ft_atof(char *s)
{
	t_info	*env;
	int		count;

	count = 0;
	if ((env = malloc(sizeof(t_info))) == NULL)
		return (0.0);
	if (s[0] == '-' && *s++)
		count = 1;
	init_value(env);
	while ((env->c = *s++) != 0 && ft_isdigit(env->c))
		env->a = env->a * 10.0 + (env->c - '0');
	check_power(env, s);
	while (env->e > 0)
	{
		env->a *= 10.0;
		env->e--;
	}
	while (env->e < 0)
	{
		env->a *= 0.1;
		env->e++;
	}
	if (count == 1)
		env->a = env->a * -1;
	return (env->a);
}
