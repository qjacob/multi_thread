/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

void	init_int_array(int *tab)
{
	int		i;

	i = -1;
	while (tab[++i])
		tab[i] = 0;
}

void	check_rotate_info(t_env *e, char **file, int i, int *count_error)
{
	(void)e;
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		ft_blue("warning: You have forgot a value. Default\
				value was init at 0.0");
	}
	if (file[i + 2] == NULL)
	{
		count_error[1] = 1;
		ft_blue("warning: You have forgot a value. Default\
				value was init at 0.0");
	}
	if (file[i + 3] == NULL)
	{
		count_error[2] = 1;
		ft_blue("warning: You have forgot a value. Default\
				value was init at 0.0");
	}
}

void	get_rotate(t_env *e, char **file, int i)
{
	int		*count_error;

	if (!(count_error = malloc(sizeof(int) * 3)))
		ft_error("error: count_error malloc failed in parse_file.c", 2);
	init_int_array(count_error);
	check_rotate_info(e, file, i, count_error);
	if (count_error[0] != 1)
		e->obj->last->rot.x = ft_atof(file[i + 1]);
	if (count_error[1] != 1)
		e->obj->last->rot.y = ft_atof(file[i + 2]);
	if (count_error[2] != 1)
		e->obj->last->rot.z = ft_atof(file[i + 3]);
}

void	default_init(t_env *e, int type)
{
	if (type == CAMERA)
	{
		set_vec(&e->camera->pos, 0.0, 0.0, 0.0);
		set_vec(&e->camera->rot, 0.0, 0.0, 0.0);
	}
	else if (type == LIGHT)
	{
		set_vec(&e->light->last->pos, 0.0, -5.0, 0.0);
		set_vec(&e->light->last->rot, 0.0, 0.0, 0.0);
		e->light->last->color = 0xFFFFFF;
	}
	else
	{
		set_vec(&e->obj->last->pos, 0.0, 0.0, 10.0);
		set_vec(&e->obj->last->rot, 0.0, 0.0, 0.0);
		set_vec(&e->obj->last->dir, 0.0, 1.0, 0.0);
		e->obj->last->radius = 5.0;
		e->obj->last->angle = 0.4;
		e->obj->last->slide = 1.0;
		e->obj->last->color = 0x00CCEE;
	}
}

void	check_light_pos(t_env *e, char **file, int i, int *count_error)
{
	(void)e;
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return ;
	}
	if (file[i + 2] == NULL)
	{
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return ;
	}
	if (file[i + 3] == NULL)
	{
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return ;
	}
}

void	get_light_pos(t_env *e, char **file, int i/*, int type*/)
{
	int			*count_error;

	if (!(count_error = malloc(sizeof(int) * 3)))
		ft_error("error: count_error malloc failed in parse_file.c", 2);
	init_int_array(count_error);
	check_light_pos(e, file, i, count_error);
	if (count_error[0] != 1)
		e->light->last->pos.x = ft_atof(file[i + 1]);
	if (count_error[1] != 1)
		e->light->last->pos.y = ft_atof(file[i + 2]);
	if (count_error[2] != 1)
		e->light->last->pos.z = ft_atof(file[i + 3]);
}

void	check_camera_pos(t_env *e, char **file, int i, int *count_error)
{
	(void)e;
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return ;
	}
	if (file[i + 2] == NULL)
	{
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return ;
	}
	if (file[i + 3] == NULL)
	{
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return ;
	}
}

void	get_camera_pos(t_env *e, char **file, int i/*, int type*/)
{
	int		*count_error;

	if (!(count_error = malloc(sizeof(int) * 3)))
		ft_error("error: count_error malloc failed in parse_file.c", 2);
	init_int_array(count_error);
	check_camera_pos(e, file, i, count_error);
	if (count_error[0] != 1)
		e->camera->pos.x = ft_atof(file[i + 1]);
	if (count_error[1] != 1)
		e->camera->pos.y = ft_atof(file[i + 2]);
	if (count_error[2] != 1)
		e->camera->pos.z = ft_atof(file[i + 3]);
}

void	check_obj_pos(t_env *e, char **file, int i, int *count_error)
{
	(void)e;
	if (file[i + 1] == NULL)
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
	}
	if (file[i + 2] == NULL)
	{
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
	}
	if (file[i + 3] == NULL)
	{
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
	}
}

void	get_obj_pos(t_env *e, char **file, int i/*, int type*/)
{
	int		*count_error;

	if (!(count_error = malloc(sizeof(int) * 3)))
		ft_error("error: count_error malloc failed in parse_file.c", 2);
	//	default_init(e);
	init_int_array(count_error);
	check_obj_pos(e, file, i, count_error);
	if (count_error[0] != 1)
		e->obj->last->pos.x = ft_atof(file[i + 1]);
	if (count_error[1] != 1)
		e->obj->last->pos.y = ft_atof(file[i + 2]);
	if (count_error[2] != 1)
		e->obj->last->pos.z = ft_atof(file[i + 3]);
}

void	get_position(t_env *e, char **file, int i, int type)
{
	if (type == LIGHT)
		get_light_pos(e, file, i);
	else if (type == CAMERA)
		get_camera_pos(e, file, i);
	else
		get_obj_pos(e, file, i);
}

void	get_radius_value(t_env *e, char **file, int i, int *count_error)
{
	if (file[i + 1] == NULL && (e->obj->last->radius = 5.0))
	{
		count_error[0] = 1;
		ft_blue("warning: You have forgot one or more value");
	}
	if (count_error[0] != 1)
		e->obj->last->radius = ft_atof(file[i + 1]);
}

void	get_radius(t_env *e, char **file, int i, int type)
{
	int		*count_error;

	if (!(count_error = malloc(sizeof(int) * 2)))
		ft_error("error: count_error malloc failed in parse_file.c", 2);
	init_int_array(count_error);
	count_error[1] = type;
	if (type == SPHERE || type == CYLINDER)
		get_radius_value(e, file, i, count_error);
	else
	{
		e->obj->last->radius = 0.0;
		ft_blue("warning: you try to put radius value in obj who don't need.");
	}
}

int        parse_color(char *color)
{
	int        i;
	int        true_color;

	i = 0;
	while (color[i])
	{
		if (color[i] >= 'A' && color[i] <= 'F')
			color[i] -= 55; 
		else if (color[i] >= 'a' && color[i] <= 'f')
			color[i] -= 87;
		else
			color[i] -= 48;
		i++;
	}
	true_color = color[5];
	true_color += color[4] << 4;
	true_color += color[3] << 8;
	true_color += color[2] << 12;
	true_color += color[1] << 16;
	true_color += color[0] << 20;
	return (true_color);
}

void	check_hexa(t_env *e, char **file, int i, int *count_error)
{
	int		j;

	j = 0;
	while (file[i + 1][j])
	{
		if (!(file[i + 1][j] >= 'a' && file[i + 1][j] <= 'f') &&
				!(file[i + 1][j] >= '0' && file[i + 1][j] <= '9'))
		{
			ft_blue("warning: Wrong hexa format. Try to make something like this 0x[0-9 or a-f]");
			if (count_error[1] == LIGHT)
				e->light->last->color = 0xFFFFFF;
			else
				e->obj->last->color = 0xFFFFFF;
			count_error[0] = 1;
			return ;
		}
		j++;
	}
}

void	length_hexa(t_env *e, char **file, int i, int *count_error)
{
	if (file[i + 1] != NULL)
	{
		if (ft_strlen(file[i + 1]) != 6)
		{
			ft_blue("warning: Wrong hexa format. Try to make something like this 0x[0-9 or a-f]");
			if (count_error[0] == LIGHT)
				e->light->last->color = 0xFFFFFF;
			else
				e->obj->last->color = 0xFFFFFF;
			count_error[0] = 1;
		}
	}
}

void	get_color_value(t_env *e, char **file, int i, int type)
{
	int		*count_error;

	if (!(count_error = malloc(sizeof(int) * 2)))
		ft_error("error: count_error malloc failed in parse_file.c", 2);
	init_int_array(count_error);
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		ft_blue("warning: You have put an invalid hexa format. Try to make something like this 0x[0-9 or a-f]");
		if (type == LIGHT)
			e->light->last->color = 0xFFFFFF;
		else
			e->obj->last->color = 0xFFFFFF;
	}
	if (count_error[0] != 1)
	{
		length_hexa(e, file, i, count_error);
		check_hexa(e, file, i, count_error);
	}
	if (count_error[0] != 1)
	{
		if (type == LIGHT)
			e->light->last->color = parse_color(file[i + 1]);
		else if (type == CAMERA)
		{
			ft_blue("warning: You try to put color to the camera.");
			return ;
		}
		else
			e->obj->last->color = parse_color(file[i + 1]);
	}
}
	

void	get_slide(t_env *e, char **file, int i, int type)
{
	int		*count_error;

	if (!(count_error = malloc(sizeof(int) * 2)))
		ft_error("error: count_error malloc failed in parse_file.c", 2);
	init_int_array(count_error);
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		ft_blue("warning: You have forgot one or more value");
		if (type == LIGHT && type == CAMERA)
		{
			ft_blue("warning: You try to put slide to the light or camera.");
			return ;
		}	
		else
			e->obj->last->slide = 1.0;
	}
	if (count_error[0] != 1)
	{
		if (type == LIGHT && type == CAMERA)
		{
			ft_blue("warning: You try to put slide to the camera or the light.");
			return ;
		}
		else
		{
			if (type == PARABOLOID || type == PLANE)
				e->obj->last->slide = ft_atof(file[i + 1]);
			else
			{
				ft_blue("warning: You have put slide to obj who don't need");
				return ;
			}
		}
	}

}

void	check_dir(t_env *e, char **file, int i, int *count_error)
{
	(void)e;
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
	}
	if (file[i + 2] == NULL)
	{
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
	}
	if (file[i + 3] == NULL)
	{
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
	}
}

void	get_dir(t_env *e, char **file, int i/*, int type*/)
{
	int		*count_error;

	if (!(count_error = malloc(sizeof(int) * 3)))
		ft_error("error: count_error malloc failed in parse_file.c", 2);
	//	default_init(e);
	init_int_array(count_error);
	check_dir(e, file, i, count_error);
	if (count_error[0] != 1)
		e->obj->last->dir.x = ft_atof(file[i + 1]);
	if (count_error[1] != 1)
		e->obj->last->dir.y = ft_atof(file[i + 2]);
	if (count_error[2] != 1)
		e->obj->last->dir.z = ft_atof(file[i + 3]);
}

void	check_if_obj(t_env *e, char **file, int *type)
{
	if (file[0] != NULL)
	{
		if (ft_strcmp(file[0], "sphere") == 0 && (*type = SPHERE))
		{
			add_obj(e, SPHERE);
			return ;
		}
		else if (ft_strcmp(file[0], "plane") == 0 && (*type = PLANE))
		{
			add_obj(e, PLANE);
			return ;
		}
		else if (ft_strcmp(file[0], "cylinder") == 0 && (*type = CYLINDER))
		{
			add_obj(e, CYLINDER);
			return ;
		}
		else if (ft_strcmp(file[0], "cone") == 0 && (*type = CONE))
		{
			add_obj(e, CONE);
			return ;
		}
		else if (ft_strcmp(file[0], "paraboloid") == 0 && (*type = PARABOLOID))
		{
			add_obj(e, PARABOLOID);
			return ;
		}
		else if (ft_strcmp(file[0], "camera") == 0 && (*type = CAMERA))
			return ;
		else if (ft_strcmp(file[0], "light") == 0 && (*type = LIGHT))
		{
			add_light(e);
			return ;
		}
		else
			ft_error("error: You have put an invalid object", 2);
	}
	else
		ft_error("error: no object found in the line", 2);
}

void	get_info(t_env *e, char *line)
{
	int		i;
	char 	**file;
	int		type;

	i = 1;
	type = 0;
	file = ft_strsplit(line, '	');
	check_if_obj(e, file, &type);
	default_init(e, type);
	while (file[i])
	{
		if (ft_strcmp(file[i], "position") == 0)
		{
			get_position(e, file, i, type);
			i += 3;
		}
		else if (ft_strcmp(file[i], "rotate") == 0)
		{
			get_rotate(e, file, i);
			i += 3;
		}
		else if (ft_strcmp(file[i], "radius") == 0)
		{
			get_radius(e, file, i, type);
			i++;
		}
		else if (ft_strcmp(file[i], "color") == 0)
		{
			get_color_value(e, file, i, type);
			i++;
		}
		else if (ft_strcmp(file[i], "slide") == 0)
		{
			get_slide(e, file, i, type);
			i++;
		}
		else if (ft_strcmp(file[i], "direction") == 0)
		{
			get_dir(e, file, i);
			i += 3;
		}
		else
			ft_error("error: You have put some invalid information.", 2);
		i++;
	}
}

void	valid_file(char *line)
{
	int		i;
	i = 0;
	while (line[i])
	{
		if (!(line[i] == '.') && !(line[i] >= 'a' && line[i] <= 'z') &&
				!(line[i] == '	') && !(line[i] >= '0' && line[i] <= '9') &&
				!(line[i] == '-'))
			ft_error("error: One or many invalid character in file", 2);
		i++;
	}
}

void	parse_line(t_env *e, char *line)
{
	valid_file(line);
	get_info(e, line);
}

void	parse_file(t_env *e, char *av)
{
	int		fd;
	int		count;
	char	*line;

	count = 0;
	if ((fd = open(av, O_RDONLY)) < 0)
		ft_error("error: Open failed in parse_file.c", 2);
	while ((get_next_line(fd, &line)) > 0)
	{
		count = 1;
		if (line[0] != '#' && line[0] != '\0')
			parse_line(e, line);
	}
	if (count == 0)
		ft_error("error: Nothing was read", 2);
	// printf("%f  %f  %f | color -> %#x\n", e->light->pos.x, e->light->pos.y, e->light->pos.z, e->light->color);
	// printf("%f  %f  %f | color -> %#x\n", e->light->last->pos.x, e->light->last->pos.y, e->light->last->pos.z, e->light->last->color);
}
