/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   diffuse.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

double	apply_specular(t_env *e, t_light *light, t_collision *clsn)
{
	double	cos_s;
	t_vec	light_dist;
	t_vec	reflect; //I - 2.0 * dot(N, I) * N.

	light_dist = sub_vec(&light->pos, &clsn->impact);
	light_dist = normalize(&light_dist);
	reflect = reflect_ray(&e->ray->dir, &clsn->normale);
	cos_s = dot(&reflect, &light_dist);
	if (cos_s < 0.0 || sub_impact(e, &clsn->impact, &light_dist, clsn))
		return (0.0);
	return (cos_s);
}

double	apply_diffuse(t_env *e, t_light *light, t_collision *clsn)
{
	double	light_ratio;
	t_vec	light_dist;
	double	cos_s;

	light_ratio = 0.0;
	light_dist = sub_vec(&light->pos, &clsn->impact);
	clsn->dist_tolight = magnitude(&light_dist);

	light_dist = normalize(&light_dist);
	cos_s = dot(&clsn->normale, &light_dist);
	if (cos_s < 0.0)
		cos_s = 0.0;
	if (!sub_impact(e, &clsn->impact, &light_dist, clsn))
		light_ratio = cos_s;
	return (light_ratio);
}

void	lighting_function(t_env *e, t_collision *clsn)
{
	double			light_ratio;
	double			light_cos;
	double			ret;
	unsigned int	l_color[3];
	t_light			*lights;

	light_ratio = 0.0;
	ret = 0.0;
	lights = e->light;
	ft_bzero(l_color, sizeof(unsigned int) * 3);
	// clsn->light_ratio = e->ambiant_light;
	while (lights != NULL)
	{
		light_cos = apply_diffuse(e, lights, clsn);
		light_ratio += light_cos;
		l_color[0] += (unsigned char)(lights->color >> 16) * light_cos;
		l_color[1] += (unsigned char)(lights->color >> 8) * light_cos;
		l_color[2] += (unsigned char)(lights->color) * light_cos;
		ret = apply_specular(e, lights, clsn);
		if (ret > clsn->spec_ratio)
			clsn->spec_ratio = ret;
		lights = lights->next;
	}
	if (e->nbr_lights != 0)
	{
		light_ratio /= e->nbr_lights;
		l_color[0] /= e->nbr_lights;
		l_color[1] /= e->nbr_lights;
		l_color[2] /= e->nbr_lights;
	}
	
	clsn->light_color = construct_color(l_color[0], l_color[1], l_color[2]);
	
	// clsn->light_ratio = light_ratio;
	clsn->spec_ratio = pow(clsn->spec_ratio, 70);
}
