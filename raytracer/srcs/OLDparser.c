/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

// int		parse_color(char *color)
// {
// 	int		i;
// 	int		true_color;

// 	i = 2;
// 	while (color[i])
// 	{
// 		if (color[i] >= 'A' && color[i] <= 'F')
// 			color[i] -= 55; 
// 		else if (color[i] >= 'a' && color[i] <= 'f')
// 			color[i] -= 87;
// 		else
// 			color[i] -= 48;
// 		i++;
// 	}
// 	true_color = color[7];
// 	true_color += color[6] << 4;
// 	true_color += color[5] << 8;
// 	true_color += color[4] << 12;
// 	true_color += color[3] << 16;
// 	true_color += color[2] << 20;
// 	return (true_color);
// }

int		add_obj_tolist(t_env *e, char *line)
{
	if (ft_strcmp("sphere", line) == 0)//ft_strchr(line, 'h') && ft_strchr(line, 's')
	{
		add_obj(e, SPHERE);
		return (SPHERE);
	}
	else if (ft_strcmp("plane", line) == 0)//ft_strchr(line, 'p') && ft_strchr(line, 'l')
	{
		add_obj(e, PLANE);
		return(PLANE);
	}
	else if (ft_strcmp("cone", line) == 0)//ft_strchr(line, 'c') && ft_strchr(line, 'o') && !ft_strchr(line, 'm')
	{
		add_obj(e, CONE);
		return(CONE);
	}
	else if (ft_strcmp("cylinder", line) == 0)
	{
		add_obj(e, CYLINDER);
		return (CYLINDER);
	}
	else if (ft_strcmp("paraboloid", line) == 0)
	{
		add_obj(e, PARABOLOID);
		return (PARABOLOID);
	}
	else if (ft_strcmp("light", line) == 0)
	{
		add_light(e);
		return (LIGHT);
	}
	// else if (ft_strchr(line, 'm'))
	// 	return (CAMERA);
	return (0);
}

void	fill_obj(t_env *e, char *line, int type)
{
	char	**param;
	char	**val;

	param = ft_strsplit(line, '\t');
	val = ft_strsplit(param[1], ' ');
	if (type == SPHERE)
	{
		if (ft_strchr(line, 'p'))
			set_vec(&e->obj->last->pos, atof(val[0]), atof(val[1]), atof(val[2]));
		else if (ft_strchr(line, 'u'))
			e->obj->last->radius = atof(val[0]);
		else
			e->obj->last->color = parse_color(param[1]);
	}
	else if (type == PLANE)
	{
		if (ft_strchr(line, 'p'))
			set_vec(&e->obj->last->pos, atof(val[0]), atof(val[1]), atof(val[2]));
		else if (ft_strchr(line, 'd'))
			e->obj->last->d = atof(val[0]);
		else
			e->obj->last->color = parse_color(param[1]);
	}
	else if (type == CONE)
	{
		if (ft_strchr(line, 'p'))
			set_vec(&e->obj->last->pos, atof(val[0]), atof(val[1]), atof(val[2]));
		else if (ft_strchr(line, 'd'))
			set_vec(&e->obj->last->dir, atof(val[0]), atof(val[1]), atof(val[2]));
		else if (ft_strchr(line, 'a'))
			e->obj->last->angle = atof(val[0]);
		else
			e->obj->last->color = parse_color(param[1]);
	}
	else if (type == CYLINDER)
	{
		if (ft_strchr(line, 'p'))
			set_vec(&e->obj->last->pos, atof(val[0]), atof(val[1]), atof(val[2]));
		else if (ft_strchr(line, 'd') && !ft_strchr(line, 'u'))
			set_vec(&e->obj->last->dir, atof(val[0]), atof(val[1]), atof(val[2]));
		else if (ft_strchr(line, 'u'))
			e->obj->last->radius = atof(val[0]);
		else
			e->obj->last->color = parse_color(param[1]);
	}
	else if (type == PARABOLOID)
	{
		if (ft_strchr(line, 'p'))
		{
			set_vec(&e->obj->last->pos, atof(val[0]), atof(val[1]), atof(val[2]));
		}
		else if (ft_strchr(line, 'd') && ft_strchr(line, 'r'))
		{
			set_vec(&e->obj->last->dir, atof(val[0]), atof(val[1]), atof(val[2]));
		}
		else if (ft_strchr(line, 'd'))
		{
			e->obj->last->d = atof(val[0]);
		}
		else
		{
			e->obj->last->color = parse_color(param[1]);
		}
	}
	else if (type == LIGHT)
	{
		if (ft_strchr(line, 'p'))
			set_vec(&e->light->last->pos, atof(val[0]), atof(val[1]), atof(val[2]));
		else
			e->light->last->color = parse_color(param[1]);
	}
	// else if (type == CAMERA)
	// {
	// 	if (ft_strchr(line, 'p'))
	// 		set_vec(&e->camera->pos, atof(val[0]), atof(val[1]), atof(val[2]));
	// 	else if (ft_strchr(line, 'z'))
	// 		e->camera->h_angle = atof(val[0]);
	// 	else if (ft_strchr(line, 'v'))
	// 		e->camera->v_angle = atof(val[0]);
	// }
}

int		open_file(int argc, char *file)
{
	int		fd;

	if (argc != 2)
		usage("\n Please link the scene like below :\n   ./rtv1 scene.txt\n");
	if ((fd = open(file, O_RDONLY)) < 0)
		usage("Opening error");
	if (read(fd, (char *)0, 0) < 0)
		usage("Opening error");
	return (fd);
}

void	parse(t_env *e, int argc, char *file)
{
	int		fd;
	char	*line;
	int		type;

	fd = open_file(argc, file);
	while (get_next_line(fd, &line) > 0)
	{
		if (line[0] != '#' && line[0] != '\0')
		{
			type = add_obj_tolist(e, line);
			while (get_next_line(fd, &line) > 0 && line[0] != '\0')
				fill_obj(e, line, type);
		}
	}
	close(fd);
}
