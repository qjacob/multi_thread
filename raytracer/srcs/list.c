/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

void	add_obj(t_env *e, int type)
{
	t_obj		*ptr;
	t_obj		*new_obj;

	new_obj = (t_obj *)malloc(sizeof(t_obj));
	if (new_obj == NULL)
		usage("Malloc error");
	new_obj->next = NULL;
	new_obj->type = type;
	ptr = e->obj;
	if (e->obj == NULL)
	{
		e->obj = new_obj;
		e->obj->last = new_obj;
	}
	else
	{
		while (ptr->next != NULL)
			ptr = ptr->next;
		ptr->next = new_obj;
		e->obj->last = new_obj;
	}
}

void	add_light(t_env *e)
{
	t_light		*ptr;
	t_light		*new_light;

	e->nbr_lights += 1;
	new_light = (t_light *)malloc(sizeof(t_light));
	if (new_light == NULL)
		usage("Malloc error");
	new_light->next = NULL;
	ptr = e->light;
	if (e->light == NULL)
	{
		e->light = new_light;
		e->light->last = new_light;
	}
	else
	{
		while (ptr->next != NULL)
			ptr = ptr->next;
		ptr->next = new_light;
		e->light->last = new_light;
	}
}
