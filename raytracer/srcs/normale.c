/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normale.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////TO CHANGE : e->ray ---> clsn->ray///////////////////////
//////////////////////////////FUNCTION/////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//N = nrm( P-C-V*(m+k) )

void	calculate_normale(t_collision *clsn, int shape, t_ray *ray)
{
	double		m;
	t_vec		pt;

	if (shape == SPHERE)
	{
		clsn->normale.x = clsn->impact.x - clsn->object->pos.x;
		clsn->normale.y = clsn->impact.y - clsn->object->pos.y;
		clsn->normale.z = clsn->impact.z - clsn->object->pos.z;
	}
	else if (shape == PLANE)
	{
		clsn->normale.x = clsn->object->pos.x;
		clsn->normale.y = clsn->object->pos.y;
		clsn->normale.z = clsn->object->pos.z;
	}
	else if (shape == CYLINDER)
	{
		pt = sub_vec(&ray->origin, &clsn->object->pos);
		m = dot(&ray->dir, &clsn->object->dir) *
		clsn->impact_d + dot(&pt, &clsn->object->dir);
		clsn->normale.x = clsn->impact.x - clsn->object->pos.x - clsn->object->dir.x * m;
		clsn->normale.y = clsn->impact.y - clsn->object->pos.y - clsn->object->dir.y * m;
		clsn->normale.z = clsn->impact.z - clsn->object->pos.z - clsn->object->dir.z * m;
	}
	else if (shape == CONE)
	{
		pt = sub_vec(&ray->origin, &clsn->object->pos);
		m = dot(&ray->dir, &clsn->object->dir) *
		clsn->impact_d + dot(&pt, &clsn->object->dir);
		clsn->normale.x = clsn->impact.x - clsn->object->pos.x -
		pow(clsn->object->angle, 2.0) * clsn->object->dir.x * m;
		clsn->normale.y = clsn->impact.y - clsn->object->pos.y -
		pow(clsn->object->angle, 2.0) * clsn->object->dir.y * m;
		clsn->normale.z = clsn->impact.z - clsn->object->pos.z -
		pow(clsn->object->angle, 2.0) * clsn->object->dir.z * m;
	}
	else if (shape == PARABOLOID)
	{
		m = dot(&ray->dir, &clsn->object->dir) *
		clsn->impact_d + dot(&pt, &clsn->object->dir);
		clsn->normale.x = clsn->impact.x - clsn->object->pos.x - clsn->object->dir.x * (m + clsn->object->slide);
		clsn->normale.y = clsn->impact.y - clsn->object->pos.y - clsn->object->dir.y * (m + clsn->object->slide);
		clsn->normale.z = clsn->impact.z - clsn->object->pos.z - clsn->object->dir.z * (m + clsn->object->slide);
	}
	clsn->normale = normalize(&clsn->normale);
}
