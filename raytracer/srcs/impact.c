/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   impact.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/05/11 12:11:53 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		impact(t_env *e, t_ray *ray, t_collision *clsn)
{
	int		impact;
	t_obj	*obj;

	impact = 0;
	obj = e->obj;
	while (obj != NULL)
	{
		if (obj->type == SPHERE)
			impact += impact_sphere(obj, ray, clsn);
		if (obj->type == PLANE)
			impact += impact_plane(obj, ray, clsn);
		if (obj->type == CYLINDER)
			impact += impact_cylinder(obj, ray, clsn);
		if (obj->type == CONE)
			impact += impact_cone(obj, ray, clsn);
		if (obj->type == PARABOLOID)
			impact += impact_paraboloid(obj, ray, clsn);
		obj = obj->next;
	}
	return (impact > 0 ? 1 : 0);
}

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
///////////////////TO_DO : PARABOLOID/////////////////////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
// a   = D|D - (D|V)^2
// b/2 = D|X - D|V*( X|V + 2*k )
// c   = X|X - X|V*( X|V + 4*k )

int		impact_paraboloid(t_obj *ptr, t_ray *ray, t_collision *clsn)
{
	double		res[3];
	double		dist;
	t_vec		point;
	int			impact;

	impact = 0;
	point = sub_vec(&ray->origin, &ptr->pos);
	// printf("%f\n", dot(&point, &point) - (dot(&point, &ptr->dir) * (dot(&point, &ptr->dir) + 4.0 * ptr->d)));
	// printf("%f %f %f\n", ptr->dir.x, ptr->dir.y, ptr->dir.z);
	res[0] = dot(&ray->dir, &ray->dir) - pow(dot(&ray->dir, &ptr->dir), 2.0);
	res[1] = dot(&ray->dir, &point) - (dot(&ray->dir, &ptr->dir) * (dot(&point, &ptr->dir) + 2.0 * ptr->slide));
	res[1] *= 2.0;
	res[2] = dot(&point, &point) - (dot(&point, &ptr->dir) * (dot(&point, &ptr->dir) + 4.0 * ptr->slide));
	// printf("%f  %f  %f\n", res[0], res[1], res[2]);
	if ((dist = quadratic(res)) > 0)
		if ((clsn->impact_d < 0.0 || dist < clsn->impact_d) && dist > 0.0)
		{
			// printf("impact\n");
			fill_collision(ptr, dist, clsn, ray);
			impact = 1;
		}
	return (impact);
}


int		impact_sphere(t_obj *ptr, t_ray *ray, t_collision *clsn)
{
	double		res[3];
	double		dist;
	int			impact;

	impact = 0;
	res[0] = pow(ray->dir.x, 2.0) + pow(ray->dir.y, 2.0) +
		pow(ray->dir.z, 2.0);
	res[1] = 2.0 * (ray->dir.x * (ray->origin.x - ptr->pos.x) + ray->dir.y *
	(ray->origin.y - ptr->pos.y) + ray->dir.z * (ray->origin.z - ptr->pos.z));
	res[2] = (pow(ray->origin.x - ptr->pos.x, 2.0) +
		pow(ray->origin.y - ptr->pos.y, 2.0) + pow(ray->origin.z -
			ptr->pos.z, 2.0)) - pow(ptr->radius, 2.0);
	if ((dist = quadratic(res)) > 0)
		if ((clsn->impact_d < 0.0 || dist < clsn->impact_d) && dist > 0.0)
		{
			fill_collision(ptr, dist, clsn, ray);
			impact = 1;
		}
	return (impact);
}

int		impact_plane(t_obj *ptr, t_ray *ray, t_collision *clsn)
{
	double		dist;
	int			impact;

	impact = 0;
	dist = -1.0 * (dot(&ptr->pos, &ray->origin) + ptr->slide) / (ptr->pos.x *
		ray->dir.x + ptr->pos.y * ray->dir.y + ptr->pos.z * ray->dir.z);
	if ((clsn->impact_d < 0.0 || dist < clsn->impact_d) && dist > 0.0)
	{
		fill_collision(ptr, dist, clsn, ray);
		impact = 1;
	}
	return (impact);
}

int		impact_cylinder(t_obj *ptr, t_ray *ray, t_collision *clsn)
{
	double		res[3];
	double		dist;
	t_vec		point;
	int			impact;

	impact = 0;
	point = sub_vec(&ray->origin, &ptr->pos);
	res[0] = dot(&ray->dir, &ray->dir) - pow(dot(&ray->dir, &ptr->dir), 2.0);
	res[1] = 2.0 * (dot(&ray->dir, &point) - dot(&ray->dir, &ptr->dir) *
		dot(&point, &ptr->dir));
	res[2] = dot(&point, &point) - pow(dot(&point, &ptr->dir), 2.0) -
		pow(ptr->radius, 2.0);
	if ((dist = quadratic(res)) > 0)
		if ((clsn->impact_d < 0.0 || dist < clsn->impact_d) && dist > 0.0)
		{
			fill_collision(ptr, dist, clsn, ray);
			impact = 1;
		}
	return (impact);
}

int		impact_cone(t_obj *ptr, t_ray *ray, t_collision *clsn)
{
	double		res[3];
	double		dist;
	t_vec		point;
	int			impact;

	impact = 0;
	point = sub_vec(&ray->origin, &ptr->pos);
	res[0] = dot(&ray->dir, &ray->dir) - (1.0 + ptr->angle * ptr->angle) *
		dot(&ray->dir, &ptr->dir) * dot(&ray->dir, &ptr->dir);
	res[1] = 2.0 * (dot(&ray->dir, &point) - (1.0 + ptr->angle * ptr->angle) *
		dot(&ray->dir, &ptr->dir) * dot(&point, &ptr->dir));
	res[2] = dot(&point, &point) - (1.0 + ptr->angle * ptr->angle) *
		dot(&point, &ptr->dir) *dot(&point, &ptr->dir);
	if ((dist = quadratic(res)) > 0)
		if ((clsn->impact_d < 0.0 || dist < clsn->impact_d) && dist > 0.0)
		{
			fill_collision(ptr, dist, clsn, ray);
			impact = 1;
		}
	return (impact);
}
