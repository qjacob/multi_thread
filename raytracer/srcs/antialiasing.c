/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   antialiasing.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

unsigned char get_aa_color(t_env *e, int x_s, int y_s, int offset)
{
	int x;
	int y;
	int res_color; // cet int additionne 4 chars
	int index;

	res_color = 0;
	x = x_s;
	while (x < x_s + e->aa)
	{
		y = y_s;
		while (y < y_s + e->aa)
		{
			index = y * e->sl * e->aa + x * 4;
			res_color += e->img_data_aa[index + offset]; 
			y++;
		}
		x++;
	}
	return ((unsigned char)(res_color / (e->aa * e->aa)));
}

void	mlx_put_image_to_win(t_env *e)
{
	int x;
	int y;
	int index;

	x = 0;
	while (x < SCR_X)
	{
		y = 0;
		while (y < SCR_Y)
		{
			index = y * e->sl + x * 4;

			e->img_data[index] = get_aa_color(e, x * e->aa, y * e->aa, 0);
			e->img_data[index + 1] = get_aa_color(e, x * e->aa, y * e->aa, 1);
			e->img_data[index + 2] = get_aa_color(e, x * e->aa, y * e->aa, 2);
			y++;
		}
		x++;
	}

	mlx_put_image_to_window(e->mlx_ptr, e->win_ptr, e->img_ptr, 0, 0);
}
