/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/05/11 12:10:47 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

double	quadratic(double tab[3])
{
	double	delta;
	double	dist;
	double	t1;
	double	t2;

	dist = 0.0;
	delta = tab[1] * tab[1] - 4.0 * tab[0] * tab[2];
	// delta *= -1.0;
	// printf("D %f - %f  %f  %f\n", delta, tab[0], tab[1], tab[2]);
	if (delta > 0.0)
	{
		t1 = (-1.0 * tab[1] + sqrt(delta)) / (2.0 * tab[0]);
		t2 = (-1.0 * tab[1] - sqrt(delta)) / (2.0 * tab[0]);
		dist = t1 < t2 ? t1 : t2;
	}
	return (dist);
}

void	fill_collision(t_obj *ptr, double dist, t_collision *clsn, t_ray *ray)
{
	clsn->impact_d = dist;
	clsn->object = ptr;
	clsn->impact.x = ray->origin.x + ray->dir.x * dist;
	clsn->impact.y = ray->origin.y + ray->dir.y * dist;
	clsn->impact.z = ray->origin.z + ray->dir.z * dist;
	calculate_normale(clsn, clsn->object->type, ray);
}
