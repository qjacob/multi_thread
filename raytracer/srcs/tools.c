/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/05/11 11:55:44 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

void	usage(char *error)
{
	ft_putendl(error);
	exit(0);
}

void	ft_blue(char *s)
{
	ft_putstr(ANSI_COLOR_BLUE);
	ft_putendl(s);
}

void	ft_error(char *s, int fd)
{
	ft_putstr_fd(ANSI_COLOR_RED, fd);
	ft_putendl_fd(s, fd);
	exit(EXIT_FAILURE);
}

unsigned int		construct_color(unsigned char r, unsigned char g, unsigned char b)
{
	unsigned int		color;

	color = (b & 0x0000FF);
	color += ((g & 0x0000FF) << 8);
	color += ((r & 0x0000FF) << 16);
	return (color);
}

void	quit_program(char *str, t_env *e)
{
	mlx_destroy_image(e->mlx_ptr, e->img_ptr);
	mlx_destroy_window(e->mlx_ptr, e->win_ptr);
	usage(str);
}

void	put_pixel_to_img(t_env *e, int x, int y, int color)
{
	int		index;

	index = y * e->sl * e->aa + x * 4;

	e->img_data_aa[index] += (color & 0x0000FF);
	e->img_data_aa[index + 1] += ((color & 0x00FF00) >> 8);
	e->img_data_aa[index + 2] += ((color & 0xFF0000) >> 16);
}
