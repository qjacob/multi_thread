/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trace.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/05/11 12:13:43 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		get_color(t_env *e, int color, t_collision *clsn)
{
	int				new_color;
	unsigned char	r;
	unsigned char	g;
	unsigned char	b;

	r = (color >> 16);
	g = (color >> 8);
	b = color;

	r *= e->ambiant_light + ((double)((unsigned char)(clsn->light_color >> 16)) / 255.0) * (1.0 - e->ambiant_light);
	g *= e->ambiant_light + ((double)((unsigned char)(clsn->light_color >> 8)) / 255.0) * (1.0 - e->ambiant_light);
	b *= e->ambiant_light + ((double)((unsigned char)(clsn->light_color)) / 255.0) * (1.0 - e->ambiant_light);

	if (r + (int)(clsn->spec_ratio * 255.0) > 255)
		r = 255;
	else
		r += (int)(clsn->spec_ratio * 255.0);
	if (g + (int)(clsn->spec_ratio * 255.0) > 255)
		g = 255;
	else
		g += (int)(clsn->spec_ratio * 255.0);
	if (b + (int)(clsn->spec_ratio * 255.0) > 255)
		b = 255;
	else
		b += (int)(clsn->spec_ratio * 255.0);

	new_color = construct_color(r, g, b);

	return (new_color);
}

void	get_ray_dir(t_env *e, t_vec *vec, int x, int y)
{
	double	p_w;
	double	p_h;

	p_w = (double)(SCR_X * e->aa) / 100.0;
	p_h = (double)(SCR_Y * e->aa) / 100.0;
	vec->x = e->ray->origin.x - p_w / 2.0 + (p_w / (double)(SCR_X * e->aa) * (double)x);
	vec->y = e->ray->origin.y - p_h / 2.0 + (p_w / (double)(SCR_X * e->aa) * (double)y);
	vec->z = p_w / 2.0;
	vec->x = vec->x * cos(e->camera->h_angle) + vec->z * sin(e->camera->h_angle);
	vec->y = vec->y * cos(e->camera->v_angle) - vec->z * sin(e->camera->v_angle);
}

void	draw_pixel(t_env *e, /*int x, int y, */t_collision *clsn)
{
	t_obj	*obj;

	obj = clsn->object;
	clsn->final_color = get_color(e, obj->color, clsn);
}

void	*draw(void *thread_data)
{
	t_vec		vp;
	int			x;
	int			y;
	t_ray		ray;
	t_env		*e;
	t_collision	*clsn;

	if (!(clsn = malloc(sizeof(t_collision))))
		ft_error("error: Clsn malloc failed in trace.c", 2);
	e = ((t_thread_data *)thread_data)->e;
	*clsn = (*e->clsn);
	set_vec(&ray.origin, 0.0, 0.0, 0.0);
	x = 0;
	while (x < (SCR_X * e->aa))
	{
		y = ((t_thread_data *)thread_data)->y_start;
		while (y < ((t_thread_data *)thread_data)->y_end)
		{
			get_ray_dir(e, &vp, x, y);
			ray.dir = normalize(&vp);
			if (impact(e, &ray, clsn))
			{
				lighting_function(e, clsn);
				/*draw_pixel(e, e->clsn);*/
				clsn->final_color = get_color(e, clsn->object->color, clsn);
				put_pixel_to_img(e, x, y, clsn->final_color);
			}
			clsn->impact_d = -1.0;
			clsn->spec_ratio = 0.0;
			y++;
		}
		x++;
	}
	return NULL;
}

void	trace(t_env *e)
{
	thr_t			thr;
	int				thread_id;
	t_thread_data	*thread_data;
	
	thread_id = 0;
	pthread_mutex_init(&e->mutex, NULL);
	while (thread_id < THREAD_LIMIT)
	{
		if (!(thread_data = (t_thread_data *)malloc(sizeof(t_thread_data))))
			ft_error("error: Clsn malloc failed in trace.c", 2);
		thread_data->y_start = thread_id * (SCR_Y * e->aa / THREAD_LIMIT);
		thread_data->y_end = (thread_id + 1) * (SCR_Y * e->aa / THREAD_LIMIT);
		thread_data->e = e;
		
		pthread_create(&thr.thr_id[thread_id], NULL, &draw, thread_data);
	
		thread_id++;
	}
	
	thread_id = 0;
	while (thread_id < THREAD_LIMIT)
	{
		pthread_join(thr.thr_id[thread_id], NULL);
		thread_id++;
	}
	return ;
	// thr_t	thr;

	// pthread_mutex_init(&e->mutex, NULL);
	// // e->x = 0;
	// // e->end_x
	// pthread_create(&thr.one, NULL, &draw0, e);
	// // e->x = 
	// pthread_create(&thr.two, NULL, &draw1, e);
	// pthread_create(&thr.three, NULL, &draw2, e);
	// pthread_create(&thr.four, NULL, &draw3, e);
	// pthread_join(thr.one, NULL);
	// pthread_join(thr.two, NULL);
	// pthread_join(thr.three, NULL);
	// pthread_join(thr.four, NULL);
	// return ;
}