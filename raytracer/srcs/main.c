/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/05/11 12:17:08 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"
#include <stdio.h>

void	screenshot(char *data)
{
	int fd;

	fd = open("screenshot.bmp", O_CREAT | O_RDWR);
	write(fd, data, SCR_Y * SCR_X * 4);
	// write(fd, data, ft_strlen(data));
	printf("%s\n", data);
}

void	init(t_env *e)
{
	e->ambiant_light = 0.10;
	e->camera = (t_camera *)malloc(sizeof(t_camera));
	e->camera->pos.x = 0.0;
	e->camera->pos.y = 0.0;
	e->camera->pos.z = 0.0;
	e->camera->h_angle = 0.0;
	e->camera->v_angle = 0.0;
	e->ray = (t_ray *)malloc(sizeof(t_ray));
	e->ray->origin.x = e->camera->pos.x;
	e->ray->origin.y = e->camera->pos.y;
	e->ray->origin.z = e->camera->pos.z;
	e->clsn = (t_collision *)malloc(sizeof(t_collision));
	e->clsn->impact_d = -1.0;
	e->rflct = (t_collision *)malloc(sizeof(t_collision));
	e->rflct->impact_d = -1.0;
	e->nbr_lights = 0;
	e->clsn->spec_ratio = 0.0;
	e->obj = NULL;
	e->light = NULL;
	e->aa = 3;
}

int		expose_hook(t_env *e)
{
	mlx_put_image_to_win(e);
	return (0);
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == KEY_ESC)
		quit_program("End of program", e);
	if (keycode == KEY_UP && e->light != NULL)
		e->light->pos.y -= 1.0;
	if (keycode == KEY_DOWN && e->light != NULL)
		e->light->pos.y += 1.0;
	if (keycode == KEY_LEFT && e->light != NULL)
		e->light->pos.x -= 1.0;
	if (keycode == KEY_RIGHT && e->light != NULL)
		e->light->pos.x += 1.0;

	ft_bzero(e->img_data_aa, SCR_X * SCR_Y * e->aa * e->aa * 4);
	trace(e);
	mlx_put_image_to_win(e);
	// screenshot(e->img_data);
	return (0);
}

int		main(int argc, char **argv)
{
	t_env	*e;

	if (argc != 2)
		ft_error("\n Please link the scene like below :\n   ./rtv1 scene.txt\n", 2);
	e = (t_env *)malloc(sizeof(t_env));
	init(e);
	parse_file(e, argv[1]);
	printf("parser achieved\n");
	if ((e->mlx_ptr = mlx_init()) == NULL)
		return (1);
	if ((e->win_ptr = mlx_new_window(e->mlx_ptr, SCR_X, SCR_Y, "Ray Tracer"))
		== NULL)
		return (1);
	e->img_ptr = mlx_new_image(e->mlx_ptr, SCR_X, SCR_Y);
	e->img_data = mlx_get_data_addr(e->img_ptr, &(e->bpp), &(e->sl), &(e->end));
	e->img_data_aa = (unsigned char *)malloc(sizeof(unsigned char) * SCR_X * SCR_Y * e->aa * e->aa * 4);
	ft_bzero(e->img_data_aa, SCR_X * SCR_Y * e->aa * e->aa * 4);
	trace(e);
	mlx_expose_hook(e->win_ptr, expose_hook, e);
	mlx_key_hook(e->win_ptr, key_hook, e);
	mlx_loop(e->mlx_ptr);
	return (0);
}
