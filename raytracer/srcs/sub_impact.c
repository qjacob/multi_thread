/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sub_impact.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		sub_impact(t_env *e, t_vec *origin, t_vec *dir, t_collision *clsn)
{
	int		impact;
	t_obj	*obj;

	impact = 0;
	obj = e->obj;
	while (obj != NULL)
	{
		if (obj->type == SPHERE)
			impact += sub_impact_sphere(obj, origin, dir, clsn);
		if (obj->type == PLANE)
			impact += sub_impact_plane(obj, origin, dir, clsn);
		if (obj->type == CYLINDER)
			impact += sub_impact_cylinder(obj, origin, dir, clsn);
		if (obj->type == CONE)
			impact += sub_impact_cone(obj, origin, dir, clsn);
		obj = obj->next;
	}
	return (impact > 0 ? 1 : 0);
}

int		sub_impact_sphere(t_obj *ptr, t_vec *origin, t_vec *dir, t_collision *clsn)
{
	double		dist;
	double		res[3];

	res[0] = pow(dir->x, 2.0) + pow(dir->y, 2.0) + pow(dir->z, 2.0);
	res[1] = 2.0 * (dir->x * (origin->x - ptr->pos.x) + dir->y *
		(origin->y - ptr->pos.y) + dir->z * (origin->z - ptr->pos.z));
	res[2] = (pow(origin->x - ptr->pos.x, 2.0) + pow(origin->y -
		ptr->pos.y, 2.0) + pow(origin->z - ptr->pos.z, 2.0)) -
			pow(ptr->radius, 2.0);
	if ((dist = quadratic(res)) > 0)
		if (dist > 0.0 && dist < clsn->dist_tolight && (void *)ptr !=
			(clsn->object))
			return (1);
	return (0);
}

int		sub_impact_plane(t_obj *ptr, t_vec *origin, t_vec *dir, t_collision *clsn)
{
	double		dist;

	dist = -1.0 * (dot(&ptr->pos, origin) + ptr->slide) /
		(ptr->pos.x * dir->x + ptr->pos.y * dir->y + ptr->pos.z * dir->z);
	if (dist > 0.0 && dist < clsn->dist_tolight && (void *)ptr !=
		(clsn->object))
		return (1);
	return (0);
}

int		sub_impact_cylinder(t_obj *ptr, t_vec *origin, t_vec *dir, t_collision *clsn)
{
	double		res[3];
	double		dist;
	t_vec		point;

	point = sub_vec(origin, &ptr->pos);
	res[0] = dot(dir, dir) - pow(dot(dir, &ptr->dir), 2.0);
	res[1] = 2.0 * (dot(dir, &point) -
		dot(dir, &ptr->dir) * dot(&point, &ptr->dir));
	res[2] = dot(&point, &point) -
		pow(dot(&point, &ptr->dir), 2.0) - pow(ptr->radius, 2.0);
	if ((dist = quadratic(res)) > 0)
		if (dist > 0.0 && dist < clsn->dist_tolight && (void *)ptr !=
			(clsn->object))
			return (1);
	return (0);
}

int		sub_impact_cone(t_obj *ptr, t_vec *origin, t_vec *dir, t_collision *clsn)
{
	double		res[3];
	double		dist;
	t_vec		point;

	point = sub_vec(origin, &ptr->pos);
	res[0] = dot(dir, dir) - (1.0 + ptr->angle * ptr->angle) *
		dot(dir, &ptr->dir) * dot(dir, &ptr->dir);
	res[1] = 2.0 * (dot(dir, &point) - (1.0 + ptr->angle *
		ptr->angle) * dot(dir, &ptr->dir) *
			dot(&point, &ptr->dir));
	res[2] = dot(&point, &point) - (1.0 + ptr->angle *
		ptr->angle) * dot(&point, &ptr->dir) *
	dot(&point, &ptr->dir);
	if ((dist = quadratic(res)) > 0)
		if (dist > 0.0 && dist < clsn->dist_tolight && (void *)ptr !=
			(clsn->object))
			return (1);
	return (0);
}
